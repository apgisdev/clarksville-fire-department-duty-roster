import requests
import csv
import os
from dotenv import load_dotenv
from arcgis.gis import GIS
from arcgis.features import FeatureLayer, managers
from arcgis.features import FeatureLayerCollection

# PURPOSE
# Requests files from a gSheet
# Write files to computer
# Update already published CSV's
# Overwrite hosted layers

load_dotenv()
dataDirPath = os.getenv('scriptDataDir')

urls = [('https://docs.google.com/spreadsheets/d/e/2PACX-1vQzVOSVNgT_pWBZEcvrwWl5uiSq37M5UIqGmITcrj5ig85rcmBXwMygJeCVTP1p9tCuW6UGgNp2GGdA/pub?gid=1421096680&single=true&output=csv', os.path.join(dataDirPath, 'battalion1.csv')),
        ('https://docs.google.com/spreadsheets/d/e/2PACX-1vQzVOSVNgT_pWBZEcvrwWl5uiSq37M5UIqGmITcrj5ig85rcmBXwMygJeCVTP1p9tCuW6UGgNp2GGdA/pub?gid=1682569742&single=true&output=csv', os.path.join(dataDirPath, 'battalion2.csv')),
        ('https://docs.google.com/spreadsheets/d/e/2PACX-1vQzVOSVNgT_pWBZEcvrwWl5uiSq37M5UIqGmITcrj5ig85rcmBXwMygJeCVTP1p9tCuW6UGgNp2GGdA/pub?gid=922867040&single=true&output=csv', os.path.join(dataDirPath, 'battalion3.csv')),
        ('https://docs.google.com/spreadsheets/d/e/2PACX-1vQzVOSVNgT_pWBZEcvrwWl5uiSq37M5UIqGmITcrj5ig85rcmBXwMygJeCVTP1p9tCuW6UGgNp2GGdA/pub?gid=1820218162&single=true&output=csv', os.path.join(dataDirPath, 'cfd_notes.csv'))]

print('STARTING')
print('\n')

# saves files from urls
def writeFile(url, saveFile):
    r = requests.get(url)
    output = open(saveFile, "wb")
    output.write(r.content)
    output.close()

for url, filePath in urls:
   save = writeFile(url, filePath)
   print('\n')
   print('Saving data')
   print('-------------------')
   print(str(filePath).rsplit('\\', 1)[1])
print('\n')
print('DONE')
print('\n')

### TODO: Clean this up. Pretty sure it's more than we need.
# uploadFiles(filePath, fileName, contentID) <-- contentID = hosted layer
uploadFiles = [ (os.path.join(dataDirPath, 'battalion1.csv'), '788fd68c550f42108136193469072b74'),
                (os.path.join(dataDirPath, 'battalion2.csv'), '17991f56a62f43bf87b9fc82d5f7dbca'),
                (os.path.join(dataDirPath, 'battalion3.csv'), '273d889696644368986f64fe4f95ac20'),
                (os.path.join(dataDirPath, 'cfd_notes.csv'), 'b198be8e8dfb4c3895b46af3b885166f')]
# published_CSV(filePath, fileName, contentID) <-- contentID = published layer
published_CSV = [(os.path.join(dataDirPath, 'battalion1.csv'), '25c7e5a1579848dfbbbb66ada2c8114d'),
                 (os.path.join(dataDirPath, 'battalion2.csv'), '16dd62182c414d7a8510369999107e35'),
                 (os.path.join(dataDirPath, 'battalion3.csv'), '266390da8bc54c53aba747fd5570b687'),
                 (os.path.join(dataDirPath, 'cfd_notes.csv'), '8370b000022743e3adec81f58be9d790')]

# contentID = published_CSV <-- Update published

def update_Published_CSV(username, userpass, uploadFile ,portalURL='https://apsu-gis.maps.arcgis.com' ,contentID=''):
    gis = GIS(portalURL, username, userpass)
    # Get content of published CSV's
    getContent = gis.content.get(contentID)
    print('\n')
    print('Updating CSV')
    print('-------------------')
    print(getContent)
    getContent.update({}, dataDirPath + uploadFile)


username = os.getenv('AGO_Username')
userpass = os.getenv('AGO_Password')

print("Updating published CSV's")
print('-------------------')
for item in published_CSV:
    update_Published_CSV(username, userpass, uploadFile=item[0], contentID=item[1])
    print('Finished Updating: ', str(item[0]).rsplit("\\", 1)[1])

print('\n')
print("FINISHED UPDATING CSV's")

# contentID = hosted layer <-- Overwrite hosted layer
def overwrite_Hosted_Layer(username, userpass, uploadFile, portalURL='https://apsu-gis.maps.arcgis.com', contentID=''):
    gis = GIS(portalURL, username, userpass)
    getContent = gis.content.get(contentID)
    print('\n')
    print('Overwriting CSV')
    print('-------------------')
    print(getContent)
    overwriteFile = FeatureLayerCollection.fromitem(getContent)
    print(overwriteFile)
    overwriteFile.manager.overwrite(uploadFile)

print('Overwriting hosted layers')
print('-------------------')
for item in uploadFiles:
    overwrite_Hosted_Layer(username, userpass, uploadFile=item[0], contentID=item[1])
    print('Finished Updating: ', str(item[0]).rsplit('\\', 1)[1])
print('\n')
print('FINISHED PROCESS')