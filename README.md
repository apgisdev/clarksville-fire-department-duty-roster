# OBJECTIVE 
+ Scrapes multiple sheets from gSheet urls
+ Saves files to computer
+ Updates already published CSV's
+ Overwrites hosted layers

# .ENV SETUP
+ AGO_Username=
+ AGO_Password=
# File path you want
+ scriptDataDir=

# IMPORTS
+ requests
+ csv
+ os
+ dotenv
+ arcgis.gis import GIS
+ from arcgis.features import FeatureLayer
+ from arcgis.features import FeatureLayerCollection

# DEPLOYMENT
## Windows
Make sure that you create the `log.txt` file beforehand and
make sure that the account that will be running the task has
full control over `log.txt`. Also ensure that same account
has full control over the `.csv` files. When scheduling the
task, make sure to use the `.bat` file for logging.